export const black = '#242424';
export const blue = '#3B97D3';
export const red = '#FFA5A5';
export const white = '#ffffff';

export const bodyBackground = '#474B54';

export const primary = '#3D4149';
export const text = '#DDDDDD';

export const yellow = '#d9cd5f';
export const lightGreen = '#96ea9a';

export const borderColor = '#2a2d32';

export const greenButton = '#96ea9a';
export const greenButtonHover = '#78bb7b';
export const blueButton = '#7dc0e7';
export const blueButtonHover = '#649ab9';

// export const loading = {
//   'overflow-y': 'hidden !important',
//   '&:after': {
//     content: '""',
//     display: 'block',
//     width: '100%',
//     height: '100%',
//     'position': 'absolute',
//     top: 0,
//     left: 0,
//     backgroundSize: 40,
//     /* tslint:disable-next-line:max-line-length */
// tslint:disable-next-line:max-line-length
//     background: `${primary} url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"> <defs> <style> .cls-1, .cls-2 { fill: none; stroke-miterlimit: 10; stroke-width: 4px; } .cls-1 { stroke: #83868d; } .cls-2 { stroke: #ddd; opacity: 0.85; } @keyframes mymove { from {transform: rotate(0deg);} to {transform: rotate(360deg);} } svg { animation: 2s linear 0s infinite mymove } </style> </defs> <g id="ic_loader" transform="translate(40) rotate(90)"> <g id="Слой_1" data-name="Слой 1"> <circle id="Ellipse_4328" data-name="Ellipse 4328" class="cls-1" cx="18" cy="18" r="18" transform="translate(2 2)"/> <path id="Path_91" data-name="Path 91" class="cls-2" d="M2,20A18,18,0,0,1,20,2"/> </g> </g> </svg>') center no-repeat`
//   }
// };
